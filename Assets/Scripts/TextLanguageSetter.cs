﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TextLanguageSetter : MonoBehaviour
{
    [SerializeField] string nameKey;
    [SerializeField] Text textElement;

    [SerializeField] TextUnitCollection units;

    void Awake()
    {
        textElement = GetComponent<Text>();
        nameKey = textElement.text;
        LocaManager.OnLanguageChange += ApplyLanguage;
    }

    void ApplyLanguage()
    {
        textElement.text = LocaManager.textUnitDict[nameKey];
    }
}
