﻿using System.Collections.Generic;
using System.Collections;
using System.Text;
using UnityEngine;
using System.IO;

public static class LocaManager  
{
    public static Dictionary<string, string> textUnitDict = new Dictionary<string, string>();

    public static event System.Action OnLanguageChange;

    public static void SetLanguage(string language)
    {
        textUnitDict.Clear();
        TextAsset textAsset = Resources.Load<TextAsset>("Localization/"+ language);

        TextUnitCollection textUnits = JsonUtility.FromJson<TextUnitCollection>(textAsset.text);

        foreach (var item in textUnits.textUnitsList)
        {
            textUnitDict.Add(item.key, item.text);
        }

        if (OnLanguageChange != null)
            OnLanguageChange();
    }
}

[System.Serializable]
public class TextUnit
{
    public string text;
    public string key;

    public TextUnit(string text, string key)
    {
        this.text = text;
        this.key = key;
    }
}
    
[System.Serializable]
public class TextUnitCollection
{
    public List<TextUnit> textUnitsList;
}