﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenuManager : MonoBehaviour 
{
    static bool isLoaded;

    void Awake()
    {
        if (!isLoaded)
        {
            LocaManager.SetLanguage("Russian");
            DataBase.LoadPrefabs();
            isLoaded = true;
        }
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
