﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public static class DataBase
{
    public static Accordance[] accordanceArray;

    public static List<Sprite> allPictures;

    public static void LoadPrefabs()
    {
        accordanceArray = Resources.LoadAll<Accordance>("AccordancePrefabs");

        allPictures = new List<Sprite>();

        foreach (var item in accordanceArray)
        {
            allPictures.Add(item.picture);
        }
    }
}
