using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class MainManager : MonoBehaviour
{
    [SerializeField] GameObject endGameElements;

    [SerializeField] AudioClip errorClip;
    [SerializeField] Image pictureSprite;
    [SerializeField] Image resultPanel;

    [SerializeField] Text successesAmount;
    [SerializeField] Text timerOutput;
    [SerializeField] Text pictureName;

    [SerializeField] float maxBlockingOffset;
    [SerializeField] float sliderSpeed;
    [SerializeField] float changeTime;

    Accordance currentAccordance;
    WaitForSeconds waitForSecs;
    AudioSource resultAudio;
    Color panelColor;

    float timer;
    float sign;

    bool isGameOver;
    bool isBlocked;
    bool isTrue;

    int successes;

    void Awake()
    {
        waitForSecs = new WaitForSeconds(changeTime);
        resultAudio = GetComponent<AudioSource>();
    }

    void Start()
    {
        timer = 3f;
        GetRandomAccordance();
    }

    void Update()
    {
        panelColor.a = Mathf.Lerp(panelColor.a, -0.1f, 3f * Time.deltaTime);

        if (timer > 0f)
            timer -= Time.deltaTime;
        else if(!isGameOver)
        {
            pictureSprite.gameObject.SetActive(false);
            endGameElements.SetActive(true);
            panelColor = Color.red;
            isGameOver = true;
            timer = 0f;
        }

        if (!isGameOver)
        {
            if (Input.touchCount > 0 && (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(0).phase == TouchPhase.Stationary))
            {
                if (!isBlocked)
                {
                    pictureSprite.transform.Translate(new Vector2(Input.GetTouch(0).deltaPosition.x, 0) * sliderSpeed * Time.deltaTime);

                    if (Mathf.Abs(pictureSprite.transform.localPosition.x) > maxBlockingOffset)
                    {
                        sign = Mathf.Sign(pictureSprite.transform.localPosition.x);
                        isBlocked = true;

                        if (isTrue == sign < 0)
                        {
                            panelColor = Color.green;
                            successes++;
                            successesAmount.text = successes.ToString();
                            timer++;
                        }
                        else
                        {
                            panelColor = Color.red;
                            resultAudio.clip = errorClip;
                            resultAudio.Play();
                            timer--;
                        }
                        panelColor.a = 0.5f;
                        StartCoroutine(SetPicture());
                    }
                }
            }
            else if (!isBlocked)
            {
                pictureSprite.transform.localPosition = Vector2.Lerp(pictureSprite.transform.localPosition, Vector2.zero, 12f * Time.deltaTime);
            }

            if (isBlocked)
            {
                pictureSprite.transform.Translate(new Vector2(1f * sign, 0f) * 5000f * Time.deltaTime);
            }
        }

        resultPanel.color = panelColor;
        timerOutput.text = timer.ToString("0.000");
    }

    IEnumerator SetPicture()
    {
        yield return waitForSecs;

        GetRandomAccordance();
        pictureSprite.transform.localPosition = Vector2.zero;
        isBlocked = false;
    }

    void GetRandomAccordance()
    {
        currentAccordance = DataBase.accordanceArray[Random.Range(0, DataBase.accordanceArray.Length)];
        pictureName.text = LocaManager.textUnitDict[currentAccordance.nameKey];

        if (isTrue = (Random.Range(0, 2) == 0))
        {
            pictureSprite.sprite = currentAccordance.picture;
        }
        else
        {
            pictureSprite.sprite = DataBase.allPictures[Random.Range(0, DataBase.allPictures.Count)];
            isTrue = (pictureSprite.sprite == currentAccordance.picture);
        }
    }

    public void TryAgain()
    {
        SceneManager.LoadScene(1);
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene(0);
    }
}